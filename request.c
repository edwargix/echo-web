#include <stdlib.h>
#include <string.h>

#include "request.h"


void realloc_http_request(http_request **r)
{
  *r = realloc(*r, sizeof(**r));
  (*r)->path = NULL;
  (*r)->path_len = 0;
}

void free_http_request(http_request *r)
{
  free(r->path);
  free(r);
}

void zero_http_request(http_request *r)
{
  free(r->path);
  memset(r, 0, sizeof(*r));
}

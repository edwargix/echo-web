#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <regex.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "request.h"
#include "logger.h"


#define EOL "\r\n"
#define BUF_LEN 1024


static void eol(int cfd)
{
  write(cfd, EOL, strlen(EOL));
}


static void line(int cfd, const char *s)
{
  write(cfd, s, strlen(s));
  eol(cfd);
}


static void http_400_response(int cfd)
{
  line(cfd, "HTTP/1.0 400 Bad Request");
  line(cfd, "Content-Length: 0");
  line(cfd, "Content-Type: text/html");
  eol(cfd);
}


static void http_404_response(int cfd)
{
  line(cfd, "HTTP/1.0 404 Not Found");
  line(cfd, "Content-Length: 0");
  line(cfd, "Content-Type: text/html");
}


void send_http_response(int cfd, const http_request *req)
{
  regex_t fname_regex;
  regmatch_t matches[2];
  size_t fname_len;
  char *fname;
  int f;
  char buf[BUF_LEN];
  ssize_t buflen;
  struct stat statbuf;

  regcomp(&fname_regex, "/\\(file[0-9].html\\|image[0-9].jpg\\)", 0);

  /* need a path */
  if (req->path == NULL) {
    logger("path never parsed in request; responding with 404\n");
    http_400_response(cfd);
    return;
  }

  /* the filename must match a strict regex to prevent leaking files */
  if (regexec(&fname_regex, req->path, 2, matches, 0) == 0) {
    fname_len = matches[1].rm_eo - matches[1].rm_so;
    fname = (char *) malloc(fname_len + 1);
    fname[fname_len] = 0;
    strncpy(fname, req->path + matches[1].rm_so, fname_len);
    logger("fname match: \"%s\"\n", fname);
  } else {
    logger("invalid fname\n");
    http_404_response(cfd);
    return;
  }

  if (stat(fname, &statbuf) < 0) {
    http_404_response(cfd);
    return;
  }

  /* status line */
  line(cfd, "HTTP/1.0 200 OK");

  /* content length line */
  snprintf(buf, BUF_LEN, "Content-Length: %ld", statbuf.st_size);
  line(cfd, buf);

  /* content type line */
  if (strncmp(fname, "f", 1) == 0) /* hack to determine filetype */
    strncpy(buf, "Content-Type: text/html", BUF_LEN-1);
  else
    strncpy(buf, "Content-Type: image/jpeg", BUF_LEN-1);
  line(cfd, buf);

  eol(cfd);

  /* read and send file */
  if ((f = open(fname, O_RDONLY)) < 0) {
    perror("error opening file for response");
    return;
  }
  logger("sending requested file\n");
  while ((buflen = read(f, buf, BUF_LEN)) > 0)
    write(cfd, buf, buflen);
}

SOURCES = $(wildcard *.c)
HEADERS = $(wildcard *.h)
OBJECTS = $(SOURCES:.c=.o)

TARGET = web_server

CFLAGS = -Wall -O3

ifeq (debug,$(firstword $(MAKECMDGOALS)))
CFLAGS += -g -ggdb3 -o0
endif

$(TARGET): $(OBJECTS)
	cc $(CFLAGS) -o $@ $^

%.o: %.c
	cc $(CFLAGS) -c -o $@ $<

depend: .depend

.depend: $(SOURCES)
	rm -f .depend
	cc $(CFLAGS) -MM $^ >> .depend

include .depend

clean:
	rm -f .depend $(TARGET) $(OBJECTS)

archive: $(SOURCES) $(HEADERS) Makefile README.txt
	git archive --prefix davidflorness/ --output $(TARGET).tar.gz HEAD $^

debug: $(TARGET)

upload: $(TARGET)
	scp $(TARGET) isengard:~/$(TARGET)

.PHONY: clean archive debug upload

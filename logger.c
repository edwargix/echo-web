#include <stdbool.h>
#include <stdarg.h>
#include <stdio.h>

#include "logger.h"

bool verbose = false;

void logger(const char *fmt, ...)
{
  if (verbose) {
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
  }
}

#ifndef LOGGER_H
#define LOGGER_H
#include <stdbool.h>

extern bool verbose;

void logger(const char *fmt, ...);

#endif /* LOGGER_H */

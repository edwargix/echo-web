#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <limits.h>
#include <unistd.h>
#include <stdbool.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <regex.h>
#include <signal.h>

#include "request.h"
#include "response.h"
#include "logger.h"


#define BUF_LEN 1024


static char usage[] = "usage: %s [-v]\n";
static int lfd, cfd;


void sigterm_handler(int signum)
{
  logger("termination signal caught; closing fds...\n");
  close(cfd);
  close(lfd);
  exit(0);
}


void handle_connection(int cfd)
{
  http_request *req = NULL;
  char *line_beg, *line_end;
  regex_t get_regex;
  regex_t end_regex;
  regmatch_t matches[2];
  /* the extra byte is for null termination */
  char buf[BUF_LEN+1];
  ssize_t buflen, newlen;
  memset(buf, 0, BUF_LEN+1);

  regcomp(&get_regex, "GET \\([0-9a-zA-Z/.-]\\+\\) HTTP/1.[0-9]", 0);
  regcomp(&end_regex, "^$", 0);
  realloc_http_request(&req);

  buflen = 0;
  zero_http_request(req);
  while (true) {
    if ((newlen = read(cfd, buf + buflen, BUF_LEN-buflen)) < 0) {
      perror("failed to read from client connection");
      return;
    }

    if (newlen == 0) {
      logger("EOF from client; trying to send response\n");
      send_http_response(cfd, req);
      break;
    }

    buflen += newlen;
    line_beg = buf;
    while ((line_end = strstr(line_beg, "\r\n")) != NULL) {
      *line_end = *(line_end+1) = 0;  /* zero the \r and \n bytes so line_beg is
                                         a string */
      logger("request line: \"%s\"\n", line_beg);  /* log line of request */

      /* match GET line */
      if (regexec(&get_regex, line_beg, 2, matches, 0) == 0) {
        /* parse path into request struct */
        req->path_len = matches[1].rm_eo - matches[1].rm_so;
        req->path = realloc(req->path, req->path_len + 1);  /* +1 for null terminator */
        req->path[req->path_len] = 0;
        strncpy(req->path, line_beg + matches[1].rm_so, req->path_len);
        logger("found HTTP GET line in request; path: \"%s\"\n", req->path);
      }
      /* match end of request */
      else if (regexec(&end_regex, line_beg, 0, NULL, 0) == 0) {
        logger("found EOF in HTTP request\n");
        send_http_response(cfd, req);
        close(cfd);
        return;
      }
      line_beg = line_end + 2;  /* jump over \r and \n */
    }

    memmove(buf, line_beg, buf+buflen-line_beg);
    memset(buf+buflen-(line_beg-buf), 0, line_beg-buf);
    buflen = buf+buflen-line_beg;
  }
}


int main(int argc, char *argv[])
{
  int opt;
  struct sockaddr_in addr;
  int queue_len;

  srand(time(NULL));

  /* enable custom SIGTERM handler */
  signal(SIGTERM, sigterm_handler);
  signal(SIGINT, sigterm_handler);

  /* parse command-line arguments */
  while ((opt = getopt(argc, argv, "v")) != -1) {
    switch (opt) {
    case 'v':
      verbose = true;
      break;
    case '?':
    case ':':
    default:
      fprintf(stderr, usage, argv[0]);
      exit(1);
    }
  }

  /* create socket fd */
  if ((lfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    perror("failed to create listening socket");
    return 1;
  }
  logger("Calling Socket() assigned file descriptor %d\n", lfd);

  /* setup socket address */
  memset(&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = htonl(INADDR_ANY);

  /* find an unused port and bind the address to the socket fd */
  logger("Calling bind(%d,%p,%zu)\n", lfd, &addr, sizeof(addr));
  do {
    addr.sin_port = htons(rand() % 10000 + 1024);
  } while (bind(lfd, (struct sockaddr *) &addr, sizeof(addr)) < 0);

  printf("Using port %d\n", ntohs(addr.sin_port));

  /* listen on socket */
  queue_len = 1;
  logger("Calling listen(%d,%d)\n", lfd, queue_len);
  if (listen(lfd, queue_len) < 0) {
    perror("failed to listen on socket fd");
    return 1;
  }

  while (true) {
    logger("Calling accept(%d,NULL,NULL).\n", lfd);
    if ((cfd = accept(lfd, NULL, NULL)) < 0) {
      perror("failed to accept connection from client");
      close(lfd);
      return 1;
    }

    logger("We have received a connection on %d\n", cfd);
    handle_connection(cfd);
    close(cfd);
  }

  close(lfd);

  return 0;
}

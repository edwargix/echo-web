#ifndef RESPONSE_H
#define RESPONSE_H

#include "request.h"

void send_http_response(int cfd, const http_request *req);

#endif /* RESPONSE_H */


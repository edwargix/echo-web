#ifndef REQUEST_H
#define REQUEST_H

typedef struct {
  char *path;
  size_t path_len;
} http_request;

void realloc_http_request(http_request **);
void free_http_request(http_request *);
void zero_http_request(http_request *);


#endif /* REQUEST_H */
